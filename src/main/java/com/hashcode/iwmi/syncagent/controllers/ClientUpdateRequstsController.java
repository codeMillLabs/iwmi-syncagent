/**
 * 
 */
package com.hashcode.iwmi.syncagent.controllers;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;
import com.hashcode.iwmi.syncagent.services.SnapshotManagerService;

/**
 * Controller to handle the requests from Client.
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
@Controller
@RequestMapping(value = "replicate")
public class ClientUpdateRequstsController {

	private static final Logger logger = LoggerFactory
			.getLogger(ClientUpdateRequstsController.class);

	@Autowired
	private SnapshotManagerService snanpManagerService;

	/**
	 * Selects the home page and populates the model with a message
	 */
	@RequestMapping(value = "/updateInfo/{clientId}", method = RequestMethod.GET)
	@ModelAttribute
	public SnapshotMetaInfo updateInfo(@PathVariable String clientId) {
		logger.info("Request came to update IWMI USB client- {}", clientId);

		SnapshotMetaInfo metaInfo = snanpManagerService
				.findLatestSnapshotInfo();
		
		if(null == metaInfo) {
			metaInfo = new SnapshotMetaInfo();
			metaInfo.setCreatedTime(Calendar.getInstance().getTime());
		}
		
		logger.info("Lasted Snapshot info for client {} - {}", clientId,
				metaInfo);

		logger.info("Response successfully sent to IWMI USB client - {}",
				clientId);
		return metaInfo;
	}

	/**
	 * @param snanpManagerService
	 *            the snanpManagerService to set
	 */
	public void setSnanpManagerService(
			SnapshotManagerService snanpManagerService) {
		this.snanpManagerService = snanpManagerService;
	}

}
