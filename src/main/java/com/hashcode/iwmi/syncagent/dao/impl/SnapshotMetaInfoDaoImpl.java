/**
 * 
 */
package com.hashcode.iwmi.syncagent.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao;
import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;

/**
 * 
 * Implementation for {@link SnapshotMetaInfo}
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
@Repository
@Transactional
public class SnapshotMetaInfoDaoImpl implements SnapshotMetaInfoDao {

	private static final Logger logger = LoggerFactory
			.getLogger(SnapshotMetaInfoDaoImpl.class);

	@PersistenceContext(type = PersistenceContextType.TRANSACTION, unitName = "iwmi-agent")
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#create(com.hashcode
	 * .iwmi.syncagent.domain.SnapshotMetaInfo)
	 */
	@Override
	public void create(SnapshotMetaInfo metaInfo) {
		if (null != metaInfo) {
			em.persist(metaInfo);
			logger.info("SnapshotMetaInfo saved successfully. {}", metaInfo);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#update(com.hashcode
	 * .iwmi.syncagent.domain.SnapshotMetaInfo)
	 */
	@Override
	public void update(SnapshotMetaInfo metaInfo) {
		if (null != metaInfo) {
			em.merge(metaInfo);
			logger.info("SnapshotMetaInfo updated successfully. {}", metaInfo);
		}
	}

	private static final String findALLMetaInfoSQL = "SELECT s FROM "
			+ SnapshotMetaInfo.class.getName() + " s "
			+ " ORDER BY createdTime DESC";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#findLatestMetaInfo()
	 */
	@Override
	public SnapshotMetaInfo findLatestMetaInfo() {

		TypedQuery<SnapshotMetaInfo> query = em.createQuery(
				findALLMetaInfoSQL, SnapshotMetaInfo.class);
		query.setMaxResults(1);

		try {
			return query.getSingleResult();

		} catch (NoResultException e) {
			logger.info("No latest metainfo found");
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#remove(com.hashcode
	 * .iwmi.syncagent.domain.SnapshotMetaInfo)
	 */
	@Override
	public void remove(SnapshotMetaInfo metaInfo) {
		if (null != metaInfo) {
			em.remove(findById(metaInfo.getId()));
		}
	}
	

	/* (non-Javadoc)
	 * @see com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#findById(java.lang.Long)
	 */
	@Override
	public SnapshotMetaInfo findById(Long id) {
		return em.find(SnapshotMetaInfo.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao#findAll()
	 */
	@Override
	public List<SnapshotMetaInfo> findAll() {
		TypedQuery<SnapshotMetaInfo> query = em.createQuery(findALLMetaInfoSQL,
				SnapshotMetaInfo.class);
		return query.getResultList();
	}

	
	@Override
	public List<SnapshotMetaInfo> findLastNMetaInfo(int lastNum) {
		TypedQuery<SnapshotMetaInfo> query = em.createQuery(
				findALLMetaInfoSQL, SnapshotMetaInfo.class);
		query.setMaxResults(lastNum);

			return query.getResultList();
	}

}
