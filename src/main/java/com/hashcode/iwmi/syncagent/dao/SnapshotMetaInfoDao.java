/**
 * 
 */
package com.hashcode.iwmi.syncagent.dao;

import java.util.List;

import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;

/**
 * Data access object for SnapshotMetaInfo.
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public interface SnapshotMetaInfoDao {

	/**
	 * create new {@link SnapshotMetaInfo}
	 * 
	 * @param metaInfo
	 *            {@link SnapshotMetaInfo}
	 */
	void create(SnapshotMetaInfo metaInfo);

	/**
	 * update {@link SnapshotMetaInfo}
	 * 
	 * @param metaInfo
	 *            {@link SnapshotMetaInfo}
	 */
	void update(SnapshotMetaInfo metaInfo);

	/**
	 * Find the latest available {@link SnapshotMetaInfo}
	 * 
	 * @return {@link SnapshotMetaInfo}
	 */
	SnapshotMetaInfo findLatestMetaInfo();
	
	/**
	 * Find the {@link SnapshotMetaInfo} by Id.
	 * 
	 * @param id uniquekey
	 * @return {@link SnapshotMetaInfo}
	 */
	SnapshotMetaInfo findById(Long id);

	/**
	 * Remove the {@link SnapshotMetaInfo}
	 * 
	 * @param metaInfo
	 *            {@link SnapshotMetaInfo}
	 */
	void remove(SnapshotMetaInfo metaInfo);
	
	/**
	 * Find the last N {@link SnapshotMetaInfo} details
	 * 
	 * @param lastNum number of meta infos to back
	 * @return list of {@link SnapshotMetaInfo}
	 */
	List<SnapshotMetaInfo> findLastNMetaInfo(int lastNum);

	/**
	 * Find all available {@link SnapshotMetaInfo}.
	 * 
	 * @return list of {@link SnapshotMetaInfo}
	 */
	List<SnapshotMetaInfo> findAll();

}
