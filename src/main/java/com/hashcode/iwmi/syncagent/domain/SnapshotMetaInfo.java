package com.hashcode.iwmi.syncagent.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <p>
 * Domain class to keep the Snapshot meta information.
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
@Entity
@Table(name = "snapshot_meta_info")
public class SnapshotMetaInfo implements Serializable {

	private static final long serialVersionUID = -8285287101478320076L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "created_time", unique = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@OrderBy("createdTime DESC")
	private Date createdTime;

	@Column(name = "file_name", nullable = false, length = 150)
	private String fileName;

	@Column(name = "file_path", nullable = false, length = 400)
	private String zipFilePath;

	@ElementCollection(targetClass = Integer.class, fetch = FetchType.EAGER)
	private Map<String, Integer> maxResourceIdMap = new HashMap<String, Integer>();

	@Column(name = "zip_size")
	private long zipFileSize;

	@ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
	private List<String> otherBackupFiles = new ArrayList<String>();

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the createdTime
	 */
	public Date getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime
	 *            the createdTime to set
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the zipFilePath
	 */
	public String getZipFilePath() {
		return zipFilePath;
	}

	/**
	 * @param zipFilePath
	 *            the zipFilePath to set
	 */
	public void setZipFilePath(String zipFilePath) {
		this.zipFilePath = zipFilePath;
	}

	/**
	 * @return the maxResourceIdMap
	 */
	public Map<String, Integer> getMaxResourceIdMap() {
		return maxResourceIdMap;
	}

	/**
	 * @param maxResourceIdMap
	 *            the maxResourceIdMap to set
	 */
	public void setMaxResourceIdMap(Map<String, Integer> maxResourceIdMap) {
		this.maxResourceIdMap = maxResourceIdMap;
	}

	/**
	 * @return the zipFileSize
	 */
	public long getZipFileSize() {
		return zipFileSize;
	}

	/**
	 * @param zipFileSize
	 *            the zipFileSize to set
	 */
	public void setZipFileSize(long zipFileSize) {
		this.zipFileSize = zipFileSize;
	}

	public void addBackupFiles(String fileName) {
		otherBackupFiles.add(fileName);
	}

	/**
	 * @return the otherBackupFiles
	 */
	public List<String> getOtherBackupFiles() {
		return otherBackupFiles;
	}

	/**
	 * @param otherBackupFiles
	 *            the otherBackupFiles ato set
	 */
	public void setOtherBackupFiles(List<String> otherBackupFiles) {
		this.otherBackupFiles = otherBackupFiles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SnapshotMetaInfo [id=");
		builder.append(id);
		builder.append(", createdTime=");
		builder.append(createdTime);
		builder.append(", fileName=");
		builder.append(fileName);
		builder.append(", zipFilePath=");
		builder.append(zipFilePath);
		builder.append(", maxResourceIdMap=");
		builder.append(maxResourceIdMap);
		builder.append(", zipFileSize=");
		builder.append(zipFileSize);
		builder.append(", otherBackupFiles=");
		builder.append(otherBackupFiles);
		builder.append("]");
		return builder.toString();
	}
}
