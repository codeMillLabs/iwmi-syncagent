/**
 * 
 */
package com.hashcode.iwmi.syncagent.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.syncagent.services.IWMISyncAgentException;
import com.hashcode.iwmi.syncagent.services.SolrRequestHandler;
import com.hashcode.iwmi.syncagent.services.SolrRequestService;

/**
 * Implementation of the Solr Request Service.
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public class SolrRequestServiceImpl implements SolrRequestService {
	
	private static final Logger logger = LoggerFactory.getLogger(SolrRequestServiceImpl.class);
	
	private SolrRequestHandler solrRequestHandler;
	private List<String> resourceTypes = new ArrayList<String>();
	private String sortFieldStr = "seriel desc";
	private String filterFieldStr = "seriel";
	private String noOfRows = "1";

	/* (non-Javadoc)
	 * @see com.hashcode.iwmi.syncagent.services.SolrRequestService#getMaxResourceId()
	 */
	@Override
	public Map<String, Integer> getMaxResourceIds() throws IWMISyncAgentException {
		Map<String, Integer> maxResourceIds = new HashMap<String, Integer>();
		
		logger.info("Get Max Resource Id Map - Started");
		 for(String resource: resourceTypes) {
			 Map<String, String> params = new HashMap<String, String>();
			 params.put("q", resource);
			 params.put("fl", filterFieldStr);
			 params.put("sort", sortFieldStr);
			 params.put("rows", noOfRows);
			 
			 QueryResponse response = solrRequestHandler.handleRequest(params);
			 SolrDocumentList list = response.getResults();
			 if (list.size() > 0) {
				SolrDocument doc = list.get(0);
				String value = (String) doc.getFieldValue(filterFieldStr);
				maxResourceIds.put(resource, Integer.valueOf(value));
			 }
		 }
		logger.info("Get Max Resource Id Map - {}, Ended", maxResourceIds);
		return maxResourceIds;
	}

	/**
	 * @return the resourceTypes
	 */
	public List<String> getResourceTypes() {
		return resourceTypes;
	}

	/**
	 * @param resourceTypes the resourceTypes to set
	 */
	public void setResourceTypes(List<String> resourceTypes) {
		this.resourceTypes = resourceTypes;
	}

	/**
	 * @return the sortFieldStr
	 */
	public String getSortFieldStr() {
		return sortFieldStr;
	}

	/**
	 * @param sortFieldStr the sortFieldStr to set
	 */
	public void setSortFieldStr(String sortFieldStr) {
		this.sortFieldStr = sortFieldStr;
	}

	/**
	 * @return the filterFieldStr
	 */
	public String getFilterFieldStr() {
		return filterFieldStr;
	}

	/**
	 * @param filterFieldStr the filterFieldStr to set
	 */
	public void setFilterFieldStr(String filterFieldStr) {
		this.filterFieldStr = filterFieldStr;
	}

	/**
	 * @return the noOfRows
	 */
	public String getNoOfRows() {
		return noOfRows;
	}

	/**
	 * @param noOfRows the noOfRows to set
	 */
	public void setNoOfRows(String noOfRows) {
		this.noOfRows = noOfRows;
	}

	/**
	 * @param solrRequestHandler the solrRequestHandler to set
	 */
	public void setSolrRequestHandler(SolrRequestHandler solrRequestHandler) {
		this.solrRequestHandler = solrRequestHandler;
	}
	
}
