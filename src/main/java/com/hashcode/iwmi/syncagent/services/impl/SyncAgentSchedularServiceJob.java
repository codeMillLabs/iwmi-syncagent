/**
 * 
 */
package com.hashcode.iwmi.syncagent.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.syncagent.services.SnapshotManagerService;

/**
 *
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public class SyncAgentSchedularServiceJob {
	
	private static final Logger logger = LoggerFactory.getLogger(SyncAgentSchedularServiceJob.class);
	
	private SnapshotManagerService managerService;

	public void execute() {
		logger.info("Start Solr backup schedular");
		long start = System.currentTimeMillis();
		try {
			logger.info("Going to execute - create latest backup");
			managerService.createBackup();
			logger.info("Going to execute - remove the old archives");
			managerService.removeOldSnapshots();
		} catch (Exception e) {
		   logger.error("Error encountered in executing the schedular,", e);	
		}
		long end = System.currentTimeMillis();
		logger.info("Start Solr backup schedular - completed, [ execution time: {} ]", (end - start));
	}

	/**
	 * @param managerService the managerService to set
	 */
	public void setManagerService(SnapshotManagerService managerService) {
		this.managerService = managerService;
	} 
}
