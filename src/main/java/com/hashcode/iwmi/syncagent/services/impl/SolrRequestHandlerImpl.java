/**
 * 
 */
package com.hashcode.iwmi.syncagent.services.impl;


import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.syncagent.services.IWMISyncAgentException;
import com.hashcode.iwmi.syncagent.services.SolrRequestHandler;

/**
 *
 * Solr related queries handle by this utility class.
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public class SolrRequestHandlerImpl implements SolrRequestHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(SolrRequestHandlerImpl.class);
	
	private String sorlServerUrl = "http://localhost:8983/solr/collection1/";
	
	public QueryResponse handleRequest(Map<String, String> params) throws IWMISyncAgentException {
		
		try {
			logger.info("Request came to submit to solr");
			SolrServer solr = new CommonsHttpSolrServer(sorlServerUrl);

			ModifiableSolrParams solarParam = new ModifiableSolrParams();

			for (Map.Entry<String, String> entry : params.entrySet()) {
				solarParam.set(entry.getKey(), entry.getValue());
			}

			QueryResponse response = solr.query(solarParam);
			logger.info("Solr request successfully handled.");
			return response;

		} catch (Exception e) {
			throw new IWMISyncAgentException(
					"Error encountered in Solr request handling,", e);
		}
	}
	
	
	public static void main(String[] args){
		try {
			
			SolrRequestHandlerImpl handler = new SolrRequestHandlerImpl();
			Map<String, String> params = new HashMap<String, String>();
			 params.put("q", "system:Commsresources");
			 params.put("fl", "id");
			 params.put("sort", "id desc");
			 params.put("rows", "1");
			 
			 
			QueryResponse response = handler.handleRequest(params);
			
			 SolrDocumentList list = response.getResults();
			 if(list.size() > 0 ){
				 SolrDocument doc = list.get(0);
				 String value = (String) doc.getFieldValue("id");
				 System.out.println("VALUE :::::::::::" + value);
			 } 
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param sorlServerUrl the sorlServerUrl to set
	 */
	public void setSorlServerUrl(String sorlServerUrl) {
		this.sorlServerUrl = sorlServerUrl;
	}
	
}
