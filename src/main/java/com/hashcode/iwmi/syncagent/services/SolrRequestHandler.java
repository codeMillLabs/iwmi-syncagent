/**
 * 
 */
package com.hashcode.iwmi.syncagent.services;

import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;

/**
 *
 * Solr related queries handle by this utility class.
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public interface SolrRequestHandler {
	
	/**
	 * handleRequest
	 * 
	 * @param params
	 * @return
	 * @throws IWMISyncAgentException
	 */
	QueryResponse handleRequest(Map<String, String> params) throws IWMISyncAgentException;

}
