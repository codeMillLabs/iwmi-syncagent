/**
 * 
 */
package com.hashcode.iwmi.syncagent.services;

import java.util.Map;

/**
 *
 * Service to call the solr request and handle the response.
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public interface SolrRequestService {

	/**
	 * Retrive the Max resource Id from the solr.
	 * 
	 * @return map of max resource ids
	 */
	Map<String, Integer> getMaxResourceIds() throws IWMISyncAgentException;
}
