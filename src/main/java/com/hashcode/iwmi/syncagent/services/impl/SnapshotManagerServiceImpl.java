/**
 * 
 */
package com.hashcode.iwmi.syncagent.services.impl;

import static java.io.File.separator;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashcode.iwmi.syncagent.dao.SnapshotMetaInfoDao;
import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;
import com.hashcode.iwmi.syncagent.services.IWMISyncAgentException;
import com.hashcode.iwmi.syncagent.services.SnapshotManagerService;
import com.hashcode.iwmi.syncagent.services.SolrRequestService;

/**
 * 
 * Implementation for the Snapshot Manager Service.
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public class SnapshotManagerServiceImpl implements SnapshotManagerService {

	private static final Logger logger = LoggerFactory
			.getLogger(SnapshotManagerServiceImpl.class);

	private String solrUrl = "http://localhost:8983/solr/replication?command=backup";
	private String solrIndexPath;
	private String snapshotFilePrefix;
	private String snapshotZipLocation;
	private int noOfSolrBackups;
	private List<String> otherBackupFiles = new ArrayList<String>();
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	private SnapshotMetaInfoDao snapshotMetaInfoDao;
	private SolrRequestService solrRequestService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hashcode.iwmi.syncagent.services.SnapshotManagerService#createSnapshot
	 * ()
	 */
	@Override
	public boolean createBackup() throws IWMISyncAgentException {
		logger.info("Index folder going to backup");
		File indexFolder = new File(solrIndexPath);

		if (null != indexFolder && indexFolder.isDirectory()) {
			Date createdDate = Calendar.getInstance().getTime();
			String zipFilename = snapshotFilePrefix + sdf.format(createdDate)
					+ ".zip";
			File zipFile = makeZip(snapshotZipLocation, indexFolder,
					zipFilename);
			
			List<String> fileNames = collectOtherBackupFiles();

			Map<String, Integer> maxResourceIdMap = solrRequestService.getMaxResourceIds();

			SnapshotMetaInfo metaInfo = new SnapshotMetaInfo();
			metaInfo.setFileName(zipFilename);
			metaInfo.setZipFilePath(snapshotZipLocation);
			metaInfo.setZipFileSize(zipFile.length());
			metaInfo.setCreatedTime(createdDate);
			metaInfo.setMaxResourceIdMap(maxResourceIdMap);
			metaInfo.setOtherBackupFiles(fileNames);

			snapshotMetaInfoDao.create(metaInfo);
			logger.info("New snashot meta info created, {}", metaInfo);
			return true;
		}

		return false;
	}
	
	
	private List<String> collectOtherBackupFiles() throws IWMISyncAgentException {
		logger.info("Collect Other backup files, [ {} ]", otherBackupFiles);
		List<String> fileNames = new ArrayList<String>(otherBackupFiles.size());

		for (String source : otherBackupFiles) {
			File sourceFile = new File(source);
			File destDir = new File(snapshotZipLocation);
			try {
				FileUtils.copyFileToDirectory(sourceFile, destDir);
				fileNames.add(sourceFile.getName());
			} catch (IOException e) {
				logger.error("Error encountered in copying files [ File :"
						+ sourceFile + "]", e);
			}
		}
		return fileNames;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hashcode.iwmi.syncagent.services.SnapshotManagerService#
	 * removeOldSnapshots()
	 */
	@Override
	public void removeOldSnapshots() throws IWMISyncAgentException {
		List<SnapshotMetaInfo> metaInfos = snapshotMetaInfoDao
				.findLastNMetaInfo(noOfSolrBackups);

		for (SnapshotMetaInfo metaInfo : metaInfos) {
			String zipFileToRemove = metaInfo.getZipFilePath()
					+ metaInfo.getFileName();
			File file = new File(zipFileToRemove);
			file.delete();
			snapshotMetaInfoDao.remove(metaInfo);
			logger.info("Old snapshot deleted, {}", metaInfo.getFileName());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hashcode.iwmi.syncagent.services.SnapshotManagerService#
	 * findLatestSnapshotInfo()
	 */
	@Override
	public SnapshotMetaInfo findLatestSnapshotInfo() {
		SnapshotMetaInfo metaInfo = snapshotMetaInfoDao.findLatestMetaInfo();
		logger.info("Latest snapshot info fetched, {}", metaInfo);
		return metaInfo;
	}

	private File makeZip(String zipFileLocation, File indexFolder,
			String zipFilename) throws IWMISyncAgentException {
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		FileOutputStream fos = null;
		ZipOutputStream zip = null;
		byte[] bytes = null;
		try {
			File zipFile = new File(zipFileLocation + separator + zipFilename);
			fos = new FileOutputStream(zipFile);
			zip = new ZipOutputStream(fos);

			for (String indexFile : indexFolder.list()) {

				String filePath = indexFolder.getPath() + separator + indexFile;

				fis = new FileInputStream(new File(filePath));
				bis = new BufferedInputStream(fis);
				zip.putNextEntry(new ZipEntry(indexFile));

				int maxSize = bis.available();
				bytes = new byte[maxSize];

				while ((bis.read(bytes)) >= 0) {
					zip.write(bytes);
				}

				zip.closeEntry();
				fis.close();
			}
			return zipFile;
		} catch (Exception e) {
			throw new IWMISyncAgentException(
					"Error encountered while making the zip file,", e);

		} finally {

			try {
				if (null != fis && null != fos && null != zip && null != bis) {
					fis.close();
					bis.close();
					zip.close();
					fos.close();
				}

			} catch (IOException e) {
				logger.error("Error in closing the connections,", e);
			}
		}

	}

	/**
	 * @return the solrUrl
	 */
	public String getSolrUrl() {
		return solrUrl;
	}

	/**
	 * @param solrUrl
	 *            the solrUrl to set
	 */
	public void setSolrUrl(String solrUrl) {
		this.solrUrl = solrUrl;
	}

	/**
	 * @return the solrIndexPath
	 */
	public String getSolrIndexPath() {
		return solrIndexPath;
	}

	/**
	 * @param solrIndexPath
	 *            the solrIndexPath to set
	 */
	public void setSolrIndexPath(String solrIndexPath) {
		this.solrIndexPath = solrIndexPath;
	}

	/**
	 * @return the snapshotFilePrefix
	 */
	public String getSnapshotFilePrefix() {
		return snapshotFilePrefix;
	}

	/**
	 * @param snapshotFilePrefix
	 *            the snapshotFilePrefix to set
	 */
	public void setSnapshotFilePrefix(String snapshotFilePrefix) {
		this.snapshotFilePrefix = snapshotFilePrefix;
	}

	/**
	 * @return the snapshotZipLocation
	 */
	public String getSnapshotZipLocation() {
		return snapshotZipLocation;
	}

	/**
	 * @param snapshotZipLocation
	 *            the snapshotZipLocation to set
	 */
	public void setSnapshotZipLocation(String snapshotZipLocation) {
		this.snapshotZipLocation = snapshotZipLocation;
	}

	/**
	 * @param snapshotMetaInfoDao
	 *            the snapshotMetaInfoDao to set
	 */
	public void setSnapshotMetaInfoDao(SnapshotMetaInfoDao snapshotMetaInfoDao) {
		this.snapshotMetaInfoDao = snapshotMetaInfoDao;
	}

	/**
	 * @param noOfSolrBackups
	 *            the noOfSolrBackups to set
	 */
	public void setNoOfSolrBackups(int noOfSolrBackups) {
		this.noOfSolrBackups = noOfSolrBackups;
	}

	/**
	 * @param solrRequestService
	 *            the solrRequestService to set
	 */
	public void setSolrRequestService(SolrRequestService solrRequestService) {
		this.solrRequestService = solrRequestService;
	}


	/**
	 * @param otherBackupFiles the otherBackupFiles to set
	 */
	public void setOtherBackupFiles(List<String> otherBackupFiles) {
		this.otherBackupFiles = otherBackupFiles;
	}
	
	
}
