/**
 * 
 */
package com.hashcode.iwmi.syncagent.services;

/**
 * <p>
 * IWMI Sync Exception class
 * </p>
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 */
public final class IWMISyncAgentException extends Exception {

	private static final long serialVersionUID = 7611431408306573461L;

	/**
	 * 
	 */
	public IWMISyncAgentException() {
	}

	/**
	 * @param message
	 */
	public IWMISyncAgentException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public IWMISyncAgentException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public IWMISyncAgentException(String message, Throwable cause) {
		super(message, cause);
	}

}
