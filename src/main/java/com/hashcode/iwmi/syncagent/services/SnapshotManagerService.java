/**
 * 
 */
package com.hashcode.iwmi.syncagent.services;

import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;

/**
 * Snapshot Manager Servicer which has the functionalities of the Snapshot.
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
public interface SnapshotManagerService {

	/**
	 * Create snapshot.
	 * 
	 * @return boolean
	 * @throws IWMISyncAgentException
	 *             throws exception in any error
	 */
	boolean createBackup() throws IWMISyncAgentException;

	/**
	 * Remove old snapshots
	 * 
	 * @throws IWMISyncAgentException
	 *             throws exception in any error
	 */
	void removeOldSnapshots() throws IWMISyncAgentException;

	/**
	 * Find the latest snapshot information.
	 * 
	 * @return {@link SnapshotMetaInfo}
	 */
	SnapshotMetaInfo findLatestSnapshotInfo();

}
