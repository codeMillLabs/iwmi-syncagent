/**
 * 
 */
package com.hashcode.iwmi.syncagent.service;

import java.util.HashMap;
import java.util.Map;

import com.hashcode.iwmi.syncagent.services.IWMISyncAgentException;
import com.hashcode.iwmi.syncagent.services.SolrRequestService;

/**
 * Dummy solr request hanlder.
 *
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 *
 */
public class SolrRequestDummy implements SolrRequestService {

	/* (non-Javadoc)
	 * @see com.hashcode.iwmi.syncagent.services.SolrRequestService#getMaxResourceIds()
	 */
	@Override
	public Map<String, Integer> getMaxResourceIds()
			throws IWMISyncAgentException {
		Map<String, Integer> maxResourceMap = new HashMap<String, Integer>();
		maxResourceMap.put("SRC1", 1);
		maxResourceMap.put("SRC2", 2);
		maxResourceMap.put("SRC3", 3);
		return maxResourceMap;
	}

}
