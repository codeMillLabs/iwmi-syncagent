/**
 * 
 */
package com.hashcode.iwmi.syncagent.service;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.syncagent.services.IWMISyncAgentException;
import com.hashcode.iwmi.syncagent.services.SnapshotManagerService;

/**
 * 
 * Tester for SnapshotManager
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
public class SnapshotManagerTest {

	@Autowired
	private SnapshotManagerService snapshotManagerService;

	@Test
	public void shouldCreateSnapshot() throws IWMISyncAgentException {

		boolean iscreated = snapshotManagerService.createBackup();
		assertTrue(iscreated);
	}

}
