/**
 * 
 */
package com.hashcode.iwmi.syncagent.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hashcode.iwmi.syncagent.domain.SnapshotMetaInfo;

/**
 * 
 * Tester for {@link SnapshotMetaInfoDao}
 * 
 * @author Amila Silva
 * @contact amilasilva88@gmail.com
 * @version 1.0
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:test-context.xml" })
public class SnapshotMetaInfoDaoTest {

	@Autowired
	private SnapshotMetaInfoDao snapshotMetaInfoDao;

	@Test
	public void shouldCreate() {
		SnapshotMetaInfo metaInfo = createDummy();
		snapshotMetaInfoDao.create(metaInfo);

		SnapshotMetaInfo savedInfos = snapshotMetaInfoDao.findLatestMetaInfo();
		assertNotNull(savedInfos);
		assertNotNull(savedInfos.getOtherBackupFiles());
		assertNotNull(savedInfos.getMaxResourceIdMap());
		assertTrue(savedInfos.getOtherBackupFiles().size() == 2);
		assertNotNull(savedInfos.getMaxResourceIdMap().size() == 3);
	}

	@Test
	public void shouldUpdate() {
		SnapshotMetaInfo metaInfo = createDummy();
		snapshotMetaInfoDao.create(metaInfo);

		SnapshotMetaInfo savedInfos = snapshotMetaInfoDao.findById(metaInfo
				.getId());
		assertNotNull(savedInfos);

		Date newDate = new Date();

		savedInfos.setFileName("Test-ext.zip");
		savedInfos.setCreatedTime(newDate);
		snapshotMetaInfoDao.update(savedInfos);

		SnapshotMetaInfo updatedInfos = snapshotMetaInfoDao.findById(savedInfos
				.getId());
		assertNotNull(updatedInfos);

		assertEquals(savedInfos.getFileName(), updatedInfos.getFileName());
		assertNotNull(updatedInfos.getMaxResourceIdMap().size() == 3);
	}

	@Test
	public void shouldFetchAllData() {

		SnapshotMetaInfo metaInfo = createDummy();
		snapshotMetaInfoDao.create(metaInfo);

		List<SnapshotMetaInfo> infos = snapshotMetaInfoDao.findAll();
		assertNotNull(infos);
		assertTrue(infos.size() > 0);
	}

	@Test
	public void shouldRemove() {
		List<SnapshotMetaInfo> infos = snapshotMetaInfoDao.findAll();
		assertNotNull(infos);
		assertTrue(infos.size() >= 0);

		for (SnapshotMetaInfo info : infos) {
			snapshotMetaInfoDao.remove(info);
		}

		infos = snapshotMetaInfoDao.findAll();
		assertNotNull(infos);
		assertTrue(infos.size() == 0);
	}

	@Test
	public void shouldTestLatestFileFetch() throws ParseException {

		List<SnapshotMetaInfo> infos = snapshotMetaInfoDao.findAll();
		assertNotNull(infos);
		assertTrue(infos.size() > 0);

		for (SnapshotMetaInfo info : infos) {
			snapshotMetaInfoDao.remove(info);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		Date d1 = sdf.parse("2013-01-10");
		Date d2 = sdf.parse("2013-01-11");
		Date d3 = sdf.parse("2013-01-09");
		Date d4 = sdf.parse("2013-01-12");

		SnapshotMetaInfo snap1 = createDummy();
		snap1.setFileName("2013-01-10");
		snap1.setCreatedTime(d1);
		snapshotMetaInfoDao.create(snap1);

		SnapshotMetaInfo val = snapshotMetaInfoDao.findLatestMetaInfo();
		assertNotNull(val);
		assertEquals(snap1.getFileName(), val.getFileName());

		SnapshotMetaInfo snap2 = createDummy();
		snap1.setFileName("2013-01-11");
		snap2.setCreatedTime(d2);
		snapshotMetaInfoDao.create(snap2);

		val = snapshotMetaInfoDao.findLatestMetaInfo();
		assertNotNull(val);
		assertEquals(snap2.getFileName(), val.getFileName());

		SnapshotMetaInfo snap3 = createDummy();
		snap3.setCreatedTime(d3);
		snap1.setFileName("2013-01-09");
		snapshotMetaInfoDao.create(snap3);

		val = snapshotMetaInfoDao.findLatestMetaInfo();
		assertNotNull(val);
		assertEquals(snap2.getFileName(), val.getFileName());

		SnapshotMetaInfo snap4 = createDummy();
		snap4.setCreatedTime(d4);
		snap1.setFileName("2013-01-12");
		snapshotMetaInfoDao.create(snap4);

		val = snapshotMetaInfoDao.findLatestMetaInfo();
		assertNotNull(val);
		assertEquals(snap4.getFileName(), val.getFileName());
		
		List<SnapshotMetaInfo> metaInfos = snapshotMetaInfoDao.findLastNMetaInfo(2);
		assertNotNull(metaInfos);
		assertEquals(metaInfos.size(), 2);

	}

	private SnapshotMetaInfo createDummy() {
		Map<String, Integer> resourceIds = new HashMap<String, Integer>();
		resourceIds.put("src1", 12);
		resourceIds.put("src2", 54);
		resourceIds.put("src3", 21);
		
		SnapshotMetaInfo metaInfo = new SnapshotMetaInfo();
		metaInfo.setCreatedTime(new Date());
		metaInfo.setFileName("test.zip");
		metaInfo.setZipFilePath("fake_path");
		metaInfo.setMaxResourceIdMap(resourceIds);
		metaInfo.setZipFileSize(1024);
		metaInfo.addBackupFiles("test1.xml");
		metaInfo.addBackupFiles("test2.xml");
		return metaInfo;
	}

}
